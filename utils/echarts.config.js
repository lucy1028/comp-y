const tooltip = {
	show: false,
	axisPointer: { //去掉移动的指示线
		type: 'none'
	},
}
//字体基本样式
const commonTextStyle = {
	fontFamily: ' PingFangSC-Regular',
	fontSize: 10,
	fontWeight: 400,
}
//坐标轴在 grid 区域中的分隔线
const splitLine = {
	lineStyle: {
		type: 'dotted',
		color: ['#F1F0F0']
	}
}
let legend = {
	show: false,
	top: 0,
	textStyle: {
		...commonTextStyle,
		color: '#666',
	},
	itemWidth: 8,
	itemHeight: 2,
	itemGap: 14,
}
const grid = {
	left: '33px',
	bottom: 40
}
//堆叠柱状图
const Column = function() {
	const base = {
		color: ['#FDB968', '#1790FC'],
	}
	const yAxis = {
		name: '万人',
		nameTextStyle: {
			...commonTextStyle,
			color: "#333",
			align: 'right', //字体对齐方式
			padding: [0, 0, 0, -35]
		},
		axisLabel: {
			...commonTextStyle,
			color: '#333',
			align: 'right', //设置文字对齐方式
		},
		axisLine: {
			show: false, //是否显示坐标轴轴线
		},
		axisTick: { //是否显示坐标轴刻度
			show: false
		},
		splitLine
	}

	const xAxis = {
		axisLabel: {
			...commonTextStyle,
			color: '#333333',
		},
		axisLine: {
			show: false, //是否显示坐标轴轴线
		},
		axisTick: { //是否显示坐标轴刻度
			show: false
		},
	}
	return {
		...base,
		tooltip,
		xAxis,
		yAxis,
		legend,
		grid
	}
}();

const pieChart = {
	option: {
		title: {
			text: '本月检核问题类型占比',
			left: 'center',
			padding: [50, 0, 0, 0]
		},
		tooltip: {
			trigger: 'item'
		},
		legend: {
			show: false
		},
		color: ['#94cd79', '#fbd57f', '#8a9ed8']
	},
	series: {
		series: [{
			type: 'pie',
			radius: '65%',
			center: ['50%', '60%'],
			label: {
				formatter: '{b}:{c}\n/{d}%'
			},
			data: []
		}]
	}
}

const barChartOption = {
	color: ['#4071F3', '#91CB74'],
	padding: [44, 16, 16, 16],
	touchMoveLimit: 24,
	enableScroll: true,
	dataLabel: false,
	legend: {
		show: false
	},
	xAxis: {
		disableGrid: true,
		scrollShow: false,
		itemCount: 5,
		axisLineColor: '#F1F0F0',
		fontColor: '#666666',
		fontSize: 10
	},
	yAxis: {
		gridColor: '#F1F0F0',
		data: [{
			min: 0,
			fontSize: 10
		}]
	},
	extra: {
		column: {
			type: 'group',
			width: 10,
			activeBgColor: '#000000',
			activeBgOpacity: 0.08
		}
	}
}

const barLineChartOption = {
	color: ["#4F7CF6", "#73DEB3", '#FF8314', '#F9D92F'],
	padding: [0, 15, 0, 15],
	legend: {
		show: true,
		position: "top",
		lineHeight: 80,
		itemGap: 6,
		fontSize: 10
	},
	fontSize: 10,
	dataLabel: false,
	dataPointShapeType: 'hollow',
	enableScroll: true,
	xAxis: {
		itemCount: 5,
		scrollShow: false,
		disableGrid: true
	},
	yAxis: {
		disabled: false,
		disableGrid: false,
		splitNumber: 5,
		gridType: "dash",
		dashLength: 1,
		gridColor: "#F1F0F0",
		padding: 34,
		showTitle: true,
		data: [{
				position: 'left',
				title: '缴存金额（亿元）',
				titleOffsetX: 25,
				min: 0,
				max: 200
			},
			{
				position: 'right',
				unit: '%',
				title: '增长率',
				titleOffsetX: -5,
				min: -20,
				max: 50
			}
		]
	},
	extra: {
		mix: {
			column: {
				width: 10
			}
		}
	}
}

const ringOptions = {
	rotate: false,
	rotateLock: false,
	color: ["#73A0FA", "#73DEB3", "#7585A2"],
	padding: [5, 5, 5, 5],
	dataLabel: true,
	legend: {
		show: false,
		name: '',
		position: "top",
		lineHeight: 25
	},
	title: {
		name: "",
		fontSize: 15,
		color: "#666666",
		offsetY: -100
	},
	subtitle: {
		name: "",
		fontSize: 15,
		color: "#7cb5ec"
	},
	extra: {
		ring: {
			ringWidth: 10,
			activeOpacity: 0.5,
			activeRadius: 10,
			offsetAngle: 0,
			labelWidth: 15,
			border: true,
			borderWidth: 2,
			borderColor: "#FFFFFF",
			customRadius: 70
		}
	}
}

export {
	Column,
	pieChart,
	barChartOption,
	barLineChartOption,
	ringOptions
}
